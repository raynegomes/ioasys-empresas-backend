import { Router } from 'express';

import SessionController from './app/controllers/SessionController';
import EnterpriseController from './app/controllers/EnterpriseController';
import EnterpriseTypeController from './app/controllers/EnterpriseTypeController';

import authMiddleware from './app/middlewares/auth';

const routes = new Router();
const urlApi = 'api';
const apiV1 = 'v1';

routes.post(`/${urlApi}/${apiV1}/users/auth/sign_in`, SessionController.store);

routes.use(authMiddleware);

routes.get(`/${urlApi}/${apiV1}/enterprises`, EnterpriseController.index);
routes.get(`/${urlApi}/${apiV1}/enterprise_types`, EnterpriseTypeController.index);

export default routes;
