import Sequelize, { Model } from 'sequelize';
import bcrypt from 'bcryptjs';

class Investor extends Model {
  static init(sequelize) {
    super.init(
      {
        investor_name: Sequelize.STRING,
        email: Sequelize.STRING,
        city: Sequelize.STRING,
        country: Sequelize.STRING,
        balance: Sequelize.DECIMAL(10, 2),
        photo: Sequelize.STRING,
        password: Sequelize.VIRTUAL,
        password_hash: Sequelize.STRING,
        portfolio: Sequelize.VIRTUAL,
        portfolio_value: Sequelize.DECIMAL(10, 2),
        first_access: Sequelize.BOOLEAN,
        super_angel: Sequelize.BOOLEAN,
      },
      {
        sequelize,
      },
    );

    this.addHook('beforeSave', async (investor) => {
      if (investor.password) {
        investor.password_hash = await bcrypt.hash(investor.password, 8);
      }
    });

    return this;
  }

  static associate(models) {
    this.belongsToMany(models.Enterprise, { through: 'enterprise_investors', as: 'enterprises', foreignKey: 'investor_id' });
  }

  checkPassword(password) {
    return bcrypt.compare(password, this.password_hash);
  }
}

export default Investor;
