import Sequelize, { Model } from 'sequelize';

class EnterpriseTypes extends Model {
  static init(sequelize) {
    super.init(
      {
        enterprise_type_name: Sequelize.STRING,
      },
      {
        sequelize,
      },
    );

    return this;
  }
}

export default EnterpriseTypes;
