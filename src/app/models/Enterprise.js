import Sequelize, { Model } from 'sequelize';

class Enterprise extends Model {
  static init(sequelize) {
    super.init(
      {
        email_enterprise: Sequelize.STRING,
        facebook: Sequelize.STRING,
        twitter: Sequelize.STRING,
        linkedin: Sequelize.STRING,
        phone: Sequelize.STRING,
        own_enterprise: Sequelize.BOOLEAN,
        enterprise_name: Sequelize.STRING,
        photo: Sequelize.STRING,
        description: Sequelize.TEXT,
        city: Sequelize.STRING,
        country: Sequelize.STRING,
        value: Sequelize.INTEGER,
        share_price: Sequelize.DECIMAL(10, 2),
      },
      {
        sequelize,
      },
    );

    return this;
  }

  static associate(models) {
    this.belongsToMany(models.Investor, { through: 'enterprise_investors', as: 'investors', foreignKey: 'enterprise_id' });
    this.belongsTo(models.EnterpriseTypes, { foreignKey: 'enterprise_type_id', as: 'enterprise_types' });
  }
}

export default Enterprise;
