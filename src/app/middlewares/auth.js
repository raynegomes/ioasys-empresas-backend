import jwt from 'jsonwebtoken';
import { promisify } from 'util';

import authConfig from '../../config/auth';

export default async (req, res, next) => {
  const authHeader = req.headers['access-token'];
  const { client } = req.headers;
  const uuid = req.headers.uid;

  let errorCode = 404;
  let errorMsg = 'Page not found';

  const token = authHeader ? authHeader.replace('Bearer ', '') : '';

  if (!token || !client || !uuid) {
    errorMsg = 'You need to sign in or sign up before continuing.';
    return res.status(401).json({ errors: [errorMsg] });
  }

  try {
    errorCode = 401;
    const decoded = await promisify(jwt.verify)(token, authConfig.secret, { issuer: client, subject: uuid });

    res.header('token-type', 'Bearer');
    res.header('access-token', token);
    res.header('client', client);
    res.header('uid', uuid);
    res.header('expiry', `${decoded.exp}`);

    next();
  } catch (error) {
    return res.status(errorCode).json({ errors: [errorMsg] });
  }
};
