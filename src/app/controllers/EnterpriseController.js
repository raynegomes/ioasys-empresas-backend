import EnterpriseRepository from '../lib/repositories/enterpriseRepository';

class EnterpriseController {
  async index(req, res) {
    const { enterprise_types, name } = req.query;

    const enterprises = await EnterpriseRepository.getByNameAndTypeId(name, enterprise_types);

    return res.json({ enterprises });
  }
}

export default new EnterpriseController();
