import EnterpriseTypesRepository from '../lib/repositories/enterpriseTypeRepository';

class EnterpriseTypeController {
  async index(req, res) {
    const eTypes = await EnterpriseTypesRepository.getAllEnterpriseTypes();

    return res.json({ enterprise_types: eTypes, success: true });
  }
}

export default new EnterpriseTypeController();
