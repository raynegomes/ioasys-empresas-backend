import Sequelize from 'sequelize';
import jwt from 'jsonwebtoken';
import uuidv4 from 'uuid/v4';
import * as Yup from 'yup';

import User from '../models/Investor';
import Enterprise from '../models/Enterprise';
import authConfig from '../../config/auth';

class SessionController {
  async store(req, res) {
    const schema = Yup.object().shape({
      email: Yup.string()
        .email()
        .required(),
      password: Yup.string().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(401).json({ success: false, errors: ['Invalid login credentials. Please try again.'] });
    }

    const { email, password } = req.body;
    const investor = await User.findOne(
      {
        where: { email },
        include: [
          {
            model: Enterprise,
            as: 'enterprises',
          },
        ],
      },
    );

    if (!investor) {
      return res.status(401).json({ success: false, errors: ['Invalid login credentials. Please try again.'] });
    }

    if (!(await investor.checkPassword(password))) {
      return res.status(401).json({ success: false, errors: ['Invalid login credentials. Please try again.'] });
    }

    const {
      id,
      investor_name,
      city,
      country,
      balance,
      photo,
      portfolio_value,
      first_access,
      super_angel,
    } = investor;

    const client = uuidv4();
    const uid = email;
    const token = jwt.sign({ id }, authConfig.secret, {
      expiresIn: authConfig.expiresIn,
      issuer: client,
      subject: uid,
    });

    // atribui dados do header
    res.header('token-type', 'Bearer');
    res.header('access-token', token);
    res.header('client', client);
    res.header('uid', uid);

    delete investor.dataValues.password_hash;
    delete investor.dataValues.portfolio;
    delete investor.dataValues.enterprise_id;

    return res.json({
      investor: {
        ...investor.dataValues,
        portfolio: {
          enterprises_number: investor.enterprise ? investor.enterprise.length : 0,
          enterprises: investor.enterprise || [],
        },
        portfolio_value: parseFloat(portfolio_value).toFixed(1),
        first_access,
        super_angel,
      },
      enterprise: investor.enterprise || null,
      success: true,
    });
  }
}

export default new SessionController();
