import EnterpriseTypes from '../../models/EnterpriseTypes';

class EnterpriseTypesRepository {
  getAllEnterpriseTypes() {
    return EnterpriseTypes.findAll();
  }
}

export default new EnterpriseTypesRepository();
