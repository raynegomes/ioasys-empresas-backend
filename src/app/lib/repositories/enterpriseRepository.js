import Sequelize, { Op } from 'sequelize';

import Enterprise from '../../models/Enterprise';
import EnterpriseType from '../../models/EnterpriseTypes';

class EnterpriseRepository {
  getAll() {
    return Enterprise.findAll({
      attributes: {
        exclude: [
          'enterprise_type_id',
        ],
      },
      include: [
        {
          model: EnterpriseType,
          as: 'enterprise_types',
          attributes: ['id', 'enterprise_type_name'],
        },
      ],
    });
  }

  getByName(name) {
    return Enterprise.findAll({
      where: {
        enterprise_name: {
          [Op.iLike]: `${name}%`,
        },
      },
      attributes: {
        exclude: [
          'enterprise_type_id',
        ],
      },
      include: [
        {
          model: EnterpriseType,
          as: 'enterprise_types',
          attributes: ['id', 'enterprise_type_name'],
        },
      ],
    });
  }

  getByTypeId(enterprise_type_id) {
    return Enterprise.findAll({
      where: {
        '$enterprise_types.id$': {
          [Op.eq]: enterprise_type_id,
        },
      },
      attributes: {
        exclude: [
          'enterprise_type_id',
        ],
      },
      include: [
        {
          model: EnterpriseType,
          as: 'enterprise_types',
          attributes: ['id', 'enterprise_type_name'],
        },
      ],
    });
  }

  getByNameAndTypeId(name, enterprise_type_id) {
    if (name && !enterprise_type_id) {
      return this.getByName(name);
    }
    if (!name && enterprise_type_id) {
      return this.getByTypeId(enterprise_type_id);
    }
    if (!name && !enterprise_type_id) {
      return this.getAll();
    }

    return Enterprise.findAll({
      where: {
        [Op.and]: [
          {
            enterprise_name: {
              [Op.iLike]: `${name}%`,
            },
          },
          {
            '$enterprise_types.id$': {
              [Op.eq]: enterprise_type_id,
            },
          },
        ],

      },
      attributes: {
        exclude: [
          'enterprise_type_id',
        ],
      },
      include: [
        {
          model: EnterpriseType,
          as: 'enterprise_types',
          attributes: ['id', 'enterprise_type_name'],
        },
      ],
    });
  }
}

export default new EnterpriseRepository();
