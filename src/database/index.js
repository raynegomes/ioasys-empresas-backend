import Sequelize from 'sequelize';
import databseConfig from '../config/database';

import Investor from '../app/models/Investor';
import Enterprise from '../app/models/Enterprise';
import EnterpriseTypes from '../app/models/EnterpriseTypes';

const models = [Investor, Enterprise, EnterpriseTypes];

class Database {
  constructor() {
    this.init();
  }

  init() {
    this.connection = new Sequelize(databseConfig);

    models
      .map((model) => model.init(this.connection))
      .map((model) => model.associate && model.associate(this.connection.models));
  }
}

export default new Database();
