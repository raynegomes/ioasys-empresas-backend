module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('enterprise_types', {
    id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
    },
    enterprise_type_name: {
      type: Sequelize.STRING,
      allowNull: false,
    },
  }),

  down: (queryInterface) => queryInterface.dropTable('enterprise_types'),
};
