module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('enterprises', {
    id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
    },
    email_enterprise: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    facebook: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    twitter: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    linkedin: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    phone: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    own_enterprise: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
      allowNull: false,
    },
    enterprise_name: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    photo: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    description: {
      type: Sequelize.TEXT,
      allowNull: true,
    },
    city: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    country: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    value: {
      type: Sequelize.INTEGER,
      allowNull: true,
    },
    share_price: {
      type: Sequelize.DECIMAL(10, 2),
      allowNull: true,
    },
    enterprise_type_id: {
      type: Sequelize.INTEGER,
      references: { model: 'enterprise_types', key: 'id' },
      onUpdate: 'CASCADE',
      onDelete: 'SET NULL',
      allowNull: true,
    },
  }),

  down: (queryInterface) => queryInterface.dropTable('enterprises'),
};
