module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('investors', {
    id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
    },
    investor_name: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    city: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    country: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    balance: {
      type: Sequelize.DECIMAL(10, 2),
      allowNull: false,
    },
    photo: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    portfolio_value: {
      type: Sequelize.DECIMAL(10, 2),
      allowNull: false,
    },
    first_access: {
      type: Sequelize.BOOLEAN,
      defaulValue: true,
      allowNull: false,
    },
    super_angel: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
      allowNull: false,
    },
    email: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: true,
    },
    password_hash: {
      type: Sequelize.STRING,
      allowNull: false,
    },
  }),

  down: (queryInterface) => queryInterface.dropTable('investors'),
};
