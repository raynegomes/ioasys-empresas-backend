module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('enterprise_investors', {
    investor_id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      references: {
        model: 'investors',
        key: 'id',
      },
      onDelete: 'CASCADE',
      allowNull: false,
    },
    enterprise_id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      references: {
        model: 'enterprises',
        key: 'id',
      },
      onDelete: 'CASCADE',
      allowNull: false,
    },
  }),

  down: (queryInterface) => queryInterface.dropTable('enterprise_investors'),
};
