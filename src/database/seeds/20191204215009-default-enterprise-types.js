module.exports = {
  up: (queryInterface) => queryInterface.bulkInsert(
    'enterprise_types',
    [
      {
        id: 1,
        enterprise_type_name: 'Agro',
      },
      {
        id: 2,
        enterprise_type_name: 'Aviation',
      },
      {
        id: 3,
        enterprise_type_name: 'Biotech',
      },
      {
        id: 4,
        enterprise_type_name: 'Eco',
      },
      {
        id: 5,
        enterprise_type_name: 'Ecommerce',
      },
      {
        id: 6,
        enterprise_type_name: 'Education',
      },
      {
        id: 7,
        enterprise_type_name: 'Fashion',
      },
      {
        id: 8,
        enterprise_type_name: 'Fintech',
      },
      {
        id: 9,
        enterprise_type_name: 'Food',
      },
      {
        id: 10,
        enterprise_type_name: 'Games',
      },
      {
        id: 11,
        enterprise_type_name: 'Health',
      },
      {
        id: 12,
        enterprise_type_name: 'IOT',
      },
      {
        id: 13,
        enterprise_type_name: 'Logistics',
      },
      {
        id: 14,
        enterprise_type_name: 'Media',
      },
      {
        id: 15,
        enterprise_type_name: 'Mining',
      },
      {
        id: 16,
        enterprise_type_name: 'Products',
      },
      {
        id: 17,
        enterprise_type_name: 'Real Estate',
      },
      {
        id: 18,
        enterprise_type_name: 'Service',
      },
      {
        id: 19,
        enterprise_type_name: 'Smart City',
      },
      {
        id: 20,
        enterprise_type_name: 'Social',
      },
      {
        id: 21,
        enterprise_type_name: 'Software',
      },
      {
        id: 22,
        enterprise_type_name: 'Technology',
      },
      {
        id: 23,
        enterprise_type_name: 'Tourism',
      },
      {
        id: 24,
        enterprise_type_name: 'Transport',
      },
    ],
    {},
  ),

  down: () => {},
};
