const bcrypt = require('bcryptjs');

module.exports = {
  up: (queryInterface) => queryInterface.bulkInsert(
    'investors',
    [
      {
        investor_name: 'Teste Apple',
        email: 'testeapple@ioasys.com.br',
        city: 'BH',
        country: 'Brasil',
        balance: 1000000.0,
        photo: null,
        password_hash: bcrypt.hashSync('12341234', 8),
        portfolio_value: 1000000.0,
        first_access: true,
        super_angel: false,
      },
    ],
    {},
  ),

  down: () => {},
};
